package br.com.loopec.skeleton.controller;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;

/**
 * Copyright (C) 2013 Loop EC - All Rights Reserved
 * Created by sandoval for skeleton-vraptor3-pg
 */
@Resource
public class SkeletonController {

    @Get
    @Path("/skeleton")
    public void index() {}
}
