package br.com.loopec.skeleton.model;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

/**
 * Copyright (C) 2013 Loop EC - All Rights Reserved
 * Created by sandoval for skeleton-vraptor3-pg
 */
@Entity
@Table
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Description cannot be left blank.")
    private String description;

    public Comment(String description) {
        this.description = description;
    }

    protected Comment() {
    }
}
