# Skeleton VRaptor3

## Introduction

Skeleton for web projects using:
* Maven 3.0.3
* VRaptor 3.5.1
* Hibernate 4.1.9
* PostgreSQL 9.1
* JUnit 4.11
* Selenium HQ 2.28.0
* Mockito 1.9.5

## HowTo start a new project?

* Change artifactId, name, and description tags in pom.xml
* Change packages in src/main/java
* Change hibernate.cfg.xml to your development database (url, user and pass)
* Change line 7 in src/main/resources/log4j.properties
* Change packagesToScan property in persistence-context.xml to the class where your Entity classes are<
* Add each Entity class manually in hibernate.cfg.xml for development
* Talk is cheap, so...CODE MAN, CODE!

Developed by [Caique Peixoto](http://caiquerodrigues.github.com), [Daniel Sandoval](http://sandoval.github.com)@[Loop EC](http://loopec.com.br)